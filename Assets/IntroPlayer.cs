using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using SM = UnityEngine.SceneManagement.SceneManager;

public class IntroPlayer : MonoBehaviour
{
    private VideoPlayer _player;
    private double _videoLenght = -2;
    private AsyncOperation _sceneLoader;

    // Start is called before the first frame update
    void Start()
    {
        _player = GetComponent<VideoPlayer>();

        _player = this.GetComponent<VideoPlayer>();
        _player.url = System.IO.Path.Combine(Application.streamingAssetsPath, "Story.mp4");
        _player.Play();

        if (_player.length > 0)
            _videoLenght = _player.length;

        _sceneLoader = SM.LoadSceneAsync(3);
        _sceneLoader.allowSceneActivation = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (_player.length <= 0)
        {
            return;
        }
        else if (_videoLenght == -2)
            _videoLenght = _player.length;


        if (Input.anyKeyDown)
        {
            _videoLenght = -1;
        }

        if (_videoLenght > 0)
        {
            _videoLenght -= Time.deltaTime;
            return;
        }

        if (!_player.isPlaying || _videoLenght < 0)
        {
            LoadScene();
        }
    }

    private void LoadScene()
    {
        _sceneLoader.allowSceneActivation = true;
    }
}
