using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPreLoader : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        foreach (AudioSource audioSrc in GetComponents<AudioSource>())
            audioSrc.clip.LoadAudioData();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
