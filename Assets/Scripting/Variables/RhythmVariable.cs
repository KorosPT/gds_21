using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Variables/RhythmVariable")]
public class RhythmVariable : ScriptableObject
{
    public string Value;

    public bool write = false;

    public bool randomizeDirections = false;

    public int speedMS = 100;

    public int futureVision = 8;

    public int reactionWindow = 8;

    public AudioClip song = null;
}
