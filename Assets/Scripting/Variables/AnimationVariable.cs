using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Variables/AnimationVariable")]
public class AnimationVariable : ScriptableObject {
    public float frameIdleDelay;
    public float frameSmackDelay;

    public Sprite idleFrame1;
    public Sprite idleFrame2;

    public Sprite smackTopFrame1;
    public Sprite smackTopFrame2;

    public Sprite smackBotFrame1;
    public Sprite smackBotFrame2;

    public Sprite smackRightFrame1;
    public Sprite smackRightFrame2;

    public Sprite smackLeftFrame1;
    public Sprite smackLeftFrame2;

    public Sprite badTouchFrame;
}
