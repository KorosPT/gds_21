using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SM = UnityEngine.SceneManagement.SceneManager;

public class MainMenuManager : MonoBehaviour {

    public FloatVariable music;
    public FloatVariable sfx;

    public Slider sliderMusic;
    public Slider sliderSfx;

    private void Start() {
        sliderMusic.SetValueWithoutNotify(music.Value);
        sliderSfx.SetValueWithoutNotify(sfx.Value);
        //sliderMusic.value = music.Value;
        //sliderSfx.value = sfx.Value;
    }

    public void LoadSceneStory() {
        SM.LoadScene(2);
    }

    public void LoadSceneChallenge()
    {
        SM.LoadScene(1);
    }

    public void Exit() {
        Application.Quit();
    }

    public void UpdateMusic() {
        music.Value = sliderMusic.value;
    }

    public void UpdateSfx() {
        sfx.Value = sliderSfx.value;
    }
}
