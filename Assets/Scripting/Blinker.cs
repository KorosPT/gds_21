using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blinker : MonoBehaviour
{
    SpriteRenderer imageRend = null;

    public Color pressed = Color.green;
    public Color notPressed = Color.red;

    public BoolVariable isPressed = null;

    public FloatVariable blinkLegthMS = null;

    private Coroutine turnOffCoroutine;

    // Start is called before the first frame update
    void Start()
    {
        imageRend = GetComponent<SpriteRenderer>();
        imageRend.color = notPressed;
    }

    // Update is called once per frame
    void Update()
    {
        if (isPressed.Value)
        {
            isPressed.Value = false;
            imageRend.color = pressed;
            if (turnOffCoroutine != null)
                StopCoroutine(turnOffCoroutine);
            turnOffCoroutine = StartCoroutine(MyCoroutine());
        }
    }

    IEnumerator MyCoroutine()
    {

        yield return new WaitForSeconds(blinkLegthMS.Value / 1000f);
        imageRend.color = notPressed;
        yield return null;
    }
}
