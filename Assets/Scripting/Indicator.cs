using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Indicator : MonoBehaviour
{
    [Range(0,1)]
    public float fill = 0;
    public Color color = Color.red;
    public float blinkInterval = 1f;
    public HandType type;

    private Image _cachedImageFill;
    private float _time = 0;
    public bool _blinking = false;
    private Coroutine _blinkRoutine;

    private float _lastLerp = 1;
    private Coroutine fillInCoroutine;
    private Transform _destination;


    public FloatVariable WarningLenghtMS = null;

    // Start is called before the first frame update
    void Start()
    {
        _cachedImageFill = GetComponent<Image>();
        _cachedImageFill.color = color;
    }

    // Update is called once per frame
    void Update()
    {
        _cachedImageFill.fillAmount = fill;

        if(fill == 1 && !_blinking) {
            _blinkRoutine = StartCoroutine(Blink());
        }

        if(fill == 0 && _blinking) {
            ResetIndicator();
        }
    }

    public void StartFillIn()
    {
        if (fillInCoroutine != null)
        {
            StopCoroutine(fillInCoroutine);
        }

        fillInCoroutine = StartCoroutine(FillinOverTime(WarningLenghtMS.Value / 1000f / _lastLerp));
    }

    private IEnumerator FillinOverTime(float duration)
    {
        float time = 0;

        _lastLerp = 0;
        while (time < duration)
        {
            _lastLerp = time / duration;
            fill = _lastLerp;
            //transform.position = Vector2.Lerp(startPosition, targetPosition, _lastLerp);
            time += Time.deltaTime;
            yield return null;
        }

        _lastLerp = 1;
        fill = 0;
    }


    private void ResetIndicator() {
        fill = 0;
        _time = 0;
        _cachedImageFill.color = color;
        _blinking = false;
        if (_blinkRoutine != null) StopCoroutine(_blinkRoutine);
    }

    private IEnumerator Blink() {
        _blinking = true;
        bool reverse = true;

        while (_blinking) {
            while (_time < blinkInterval) {
                if(reverse)
                    _cachedImageFill.color = Color.Lerp(color, Color.white, _time / blinkInterval);
                else
                    _cachedImageFill.color = Color.Lerp(Color.white, color, _time / blinkInterval);
                _time += Time.deltaTime;
                yield return null;
            }
            reverse = !reverse;
            _time = 0;
        }
    }
}
