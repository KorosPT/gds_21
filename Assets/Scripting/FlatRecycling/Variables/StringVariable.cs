﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;


[CreateAssetMenu(menuName = "Variables/StringVariable")]
public class StringVariable : ScriptableObject
{
    public string Value;
}

