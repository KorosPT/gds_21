﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;


[CreateAssetMenu(menuName = "Variables/BoolVariable")]
public class BoolVariable : ScriptableObject
{
    [SerializeField]
    private bool CurrentValue;
    public bool LockVariable = false;
    public bool LockedValue = false;

    public bool Value
    {
        get
        {
            return Get();
        }
        set
        {
            Set(value);
        }
    }

    public void Set(bool value)
    {
        if (!LockVariable)
            CurrentValue = value;
        else
            CurrentValue = LockedValue;
    }

    public void Toggle()
    {
        if (!LockVariable)
            CurrentValue = !CurrentValue;
        else
            CurrentValue = LockedValue;
    }

    public bool Get()
    {
        return CurrentValue;
    }
}

