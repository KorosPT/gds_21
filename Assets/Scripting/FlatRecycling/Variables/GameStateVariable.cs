﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;


[CreateAssetMenu(menuName = "Variables/GameStateVariable")]
public class GameStateVariable : IntVariable
{
    public new GameState Value
    {
        get
        {
            return (GameState)base.Value;
        }
        set
        {
            base.Value = (int)value;
        }
    }

    public void Set(GameState value)
    {
        Value = value;
    }

    public GameState Get()
    {
        return Value;
    }

    public void Pause()
    {
        Value = GameState.Paused;
    }

    public void Run()
    {
        Value = GameState.Running;
    }
}

public enum GameState
{
    Paused,
    Running,
    Victory,
    Loss
}