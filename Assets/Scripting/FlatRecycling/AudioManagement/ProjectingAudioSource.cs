﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ProjectingAudioSource : MonoBehaviour
{
    public float volume = 1f;
    public float minDistance = 1f;
    public float maxDistance = 8f;

    public ProjectedAudioRuntimeSet requestRuntimeSet = null;

    private List<RemoteAudioRequest> ongoingRequests = null;

    void Start()
    {
        ongoingRequests = new List<RemoteAudioRequest>();
    }

    public void RequestSound(Transform worldPosition, AudioClip clip, bool ongoing = false)
    {
        RemoteAudioRequest newReq = new RemoteAudioRequest();
        newReq.source = this;
        newReq.clip = clip;
        newReq.ongoing = ongoing;

        if (ongoing)
            ongoingRequests.Add(newReq);

        requestRuntimeSet.AddRequest(ref newReq);
    }

    public void CancelOngoingRequests()
    {
        foreach (RemoteAudioRequest req in ongoingRequests)
        {
            req.ongoing = false;
            requestRuntimeSet.RemoveRequest(req);
        }

        ongoingRequests = ongoingRequests.Where(x => x.ongoing).ToList();
    }
}
