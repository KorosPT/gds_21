﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class AudioProjector : MonoBehaviour
{
    public ProjectedAudioRuntimeSet requestRuntimeSet = null;
    public BoolVariable isGameover = null;

    private Stack<GameObject> freeAudioSources = null;
    private Dictionary<RemoteAudioRequest, GameObject[]> bussyAudioSources = null;
    private Dictionary<RemoteAudioRequest, GameObject[]> ongoingAudioSources = null;

    private int playerCount = 0;

    // Start is called before the first frame update
    void Awake()
    {
        freeAudioSources = new Stack<GameObject>();
        bussyAudioSources = new Dictionary<RemoteAudioRequest, GameObject[]>();
        ongoingAudioSources = new Dictionary<RemoteAudioRequest, GameObject[]>();
        requestRuntimeSet.Init();
    }

    // Update is called once per frame
    void Update()
    {
        if (isGameover.Value)
        {
            FreeAllResources();
            return;
        }

        //Update positions and volume of currently playing sounds (one-off and ongoing)
        UpdateExisting();

        //Create new sounds
        CreateNew();

        //trash old requests
        requestRuntimeSet.currentrequests = requestRuntimeSet.currentrequests.Where(x => x.ongoing).ToList();

        //free what is no longer playing
        FreeResources();
    }

    private void FreeResources()
    {
        List<RemoteAudioRequest> doneReqs = new List<RemoteAudioRequest>();

        foreach (KeyValuePair<RemoteAudioRequest, GameObject[]> pair in bussyAudioSources)
        {
            bool done = true;
            GameObject[] audioSources = pair.Value;
            for (int i = 0; i < audioSources.Length; i++)
            {
                if (audioSources[i] == null)
                    continue;
                AudioSource au = audioSources[i].GetComponent<AudioSource>();
                if (!au.isPlaying)
                {
                    freeAudioSources.Push(audioSources[i]);
                    audioSources[i] = null;
                }
                else
                    done = false;
            }
            if (done) doneReqs.Add(pair.Key);
        }

        foreach (KeyValuePair<RemoteAudioRequest, GameObject[]> pair in ongoingAudioSources)
        {
            bool done = true;
            GameObject[] audioSources = pair.Value;
            for (int i = 0; i < audioSources.Length; i++)
            {
                if (audioSources[i] == null)
                    continue;
                AudioSource au = audioSources[i].GetComponent<AudioSource>();
                if (!au.isPlaying)
                {
                    freeAudioSources.Push(audioSources[i]);
                    audioSources[i] = null;
                }
                else
                    done = false;
            }
            if (done) doneReqs.Add(pair.Key);
        }

        foreach (RemoteAudioRequest req in doneReqs)
        {
            bussyAudioSources.Remove(req);
            ongoingAudioSources.Remove(req);
        }
    }

    private void FreeAllResources()
    {
        List<RemoteAudioRequest> doneReqs = new List<RemoteAudioRequest>();

        foreach (KeyValuePair<RemoteAudioRequest, GameObject[]> pair in bussyAudioSources)
        {
            bool done = true;
            GameObject[] audioSources = pair.Value;
            for (int i = 0; i < audioSources.Length; i++)
            {
                if (audioSources[i] == null)
                    continue;
                AudioSource au = audioSources[i].GetComponent<AudioSource>();
                au.Stop();

                freeAudioSources.Push(audioSources[i]);
                audioSources[i] = null;
            }
            if (done) doneReqs.Add(pair.Key);
        }

        foreach (KeyValuePair<RemoteAudioRequest, GameObject[]> pair in ongoingAudioSources)
        {
            bool done = true;
            GameObject[] audioSources = pair.Value;
            for (int i = 0; i < audioSources.Length; i++)
            {
                if (audioSources[i] == null)
                    continue;
                AudioSource au = audioSources[i].GetComponent<AudioSource>();
                au.Stop();

                freeAudioSources.Push(audioSources[i]);
                audioSources[i] = null;
            }
            if (done) doneReqs.Add(pair.Key);
        }

        foreach (RemoteAudioRequest req in doneReqs)
        {
            bussyAudioSources.Remove(req);
            ongoingAudioSources.Remove(req);
        }
    }

    private void CreateNew()
    {
        for (int i = 0; i < requestRuntimeSet.currentrequests.Count; i++)
        {
            RemoteAudioRequest req = requestRuntimeSet.currentrequests[i];

            GameObject[] audioSources;
            bool createdSomething = false;
            if (req.ongoing)
            {
                if (ongoingAudioSources.ContainsKey(req))
                    audioSources = ongoingAudioSources[req];
                else
                    audioSources = new GameObject[requestRuntimeSet.audioListeners.Count];
            }
            else
                audioSources = new GameObject[requestRuntimeSet.audioListeners.Count];

            for (int j = 0; j < requestRuntimeSet.audioListeners.Count; j++)
            {
                if (audioSources[j] != null)
                    continue;

                VirtualAudioListener listener = requestRuntimeSet.audioListeners[j];
                Vector3 relativePosition = listener.transform.position - req.source.transform.position;
                if (relativePosition.magnitude < req.source.maxDistance)
                {
                    createdSomething = true;
                    GameObject audioSource = getFreeAudioSource();
                    AudioSource au = audioSource.GetComponent<AudioSource>();
                    au.panStereo = listener.StereoPan;
                    au.clip = req.clip;
                    au.loop = req.ongoing;

                    UpdatePositionAndVolume(req, listener, audioSource);

                    audioSources[j] = audioSource;
                    au.Play();
                }
            }
            if (createdSomething)
            {
                if (req.ongoing)
                    ongoingAudioSources[req] = audioSources;
                else
                    bussyAudioSources[req] = audioSources;
            }
        }
    }

    private void UpdateExisting()
    {
        foreach (KeyValuePair<RemoteAudioRequest, GameObject[]> pair in bussyAudioSources)
        {
            RemoteAudioRequest req = pair.Key;
            GameObject[] audioSources = pair.Value;

            for (int i = 0; i < requestRuntimeSet.audioListeners.Count; i++)
            {
                VirtualAudioListener listener = requestRuntimeSet.audioListeners[i];
                GameObject audioSource = audioSources[i];

                if (audioSource == null)
                    continue;

                UpdatePositionAndVolume(req, listener, audioSource);
            }
        }

        foreach (KeyValuePair<RemoteAudioRequest, GameObject[]> pair in ongoingAudioSources)
        {
            RemoteAudioRequest req = pair.Key;
            GameObject[] audioSources = pair.Value;

            for (int i = 0; i < requestRuntimeSet.audioListeners.Count; i++)
            {
                VirtualAudioListener listener = requestRuntimeSet.audioListeners[i];
                GameObject audioSource = audioSources[i];

                if (audioSource == null)
                    continue;

                if (!req.ongoing)
                {
                    AudioSource au = audioSource.GetComponent<AudioSource>();
                    //au.loop = false;
                    au.Stop();
                    //requestRuntimeSet.currentrequests.Remove(req);
                    continue;
                }

                UpdatePositionAndVolume(req, listener, audioSource);
            }
        }
    }

    private GameObject getFreeAudioSource()
    {
        if (freeAudioSources.Count > 0)
            return freeAudioSources.Pop();

        return NewSoundPlayer("AudioPoolPlayer-" + playerCount++);
    }

    private void UpdatePositionAndVolume(RemoteAudioRequest req, VirtualAudioListener listener, GameObject audioSource)
    {
        AudioSource au = audioSource.GetComponent<AudioSource>();
        Vector3 relativePosition = listener.transform.position - req.source.transform.position;

        if (au.panStereo < -0.1 && relativePosition.x > 0)
            relativePosition = new Vector3(-relativePosition.x, relativePosition.y, relativePosition.z);

        if (au.panStereo > 0.1 && relativePosition.x > 0)
            relativePosition = new Vector3(-relativePosition.x, relativePosition.y, relativePosition.z);

        audioSource.transform.position = this.transform.position + (relativePosition);

        float volume = 1f - relativePosition.magnitude.Remap(req.source.minDistance, req.source.maxDistance, 0f, 1f);
        Debug.Log(volume * req.source.volume);
        au.volume = volume * req.source.volume;
    }

    public GameObject NewSoundPlayer(string goName)
    {
        GameObject soundPlayer = new GameObject("TempAudio");
        soundPlayer.name = goName;
        AudioSource au = soundPlayer.AddComponent<AudioSource>();
        au.minDistance = 14;
        au.maxDistance = 15;
        soundPlayer.transform.parent = this.transform;
        return soundPlayer;
    }
}
