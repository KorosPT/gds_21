﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AutioRuntimeSet")]
public class ProjectedAudioRuntimeSet : ScriptableObject
{
    public List<VirtualAudioListener> audioListeners = null;
    public List<RemoteAudioRequest> currentrequests = null;

    internal void RegisterVirtualListener(VirtualAudioListener virtualAudioListener)
    {
        audioListeners.Add(virtualAudioListener);
    }

    public void Init()
    {
        audioListeners = new List<VirtualAudioListener>();
        currentrequests = new List<RemoteAudioRequest>();
    }

    internal void AddRequest(ref RemoteAudioRequest newReq)
    {
        currentrequests.Add(newReq);
    }

    internal void RemoveRequest(RemoteAudioRequest req)
    {
        currentrequests.Remove(req);
    }
}
