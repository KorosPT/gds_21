﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VirtualAudioListener : MonoBehaviour
{
    public ProjectedAudioRuntimeSet requestRuntimeSet = null;

    [Range(-1.0f, 1.0f)]
    public float StereoPan = 0f;

    // Start is called before the first frame update
    void Start()
    {
        requestRuntimeSet.RegisterVirtualListener(this);
    }
}
