﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class RemoteAudioRequest
{
    public ProjectingAudioSource source = null;
    public AudioClip clip = null;
    public bool ongoing = false;
}
