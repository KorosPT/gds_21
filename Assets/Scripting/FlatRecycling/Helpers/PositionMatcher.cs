﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PositionMatcher : MonoBehaviour
{

    public Transform SourceTransform;

    public bool ignoreX = true;
    public bool ignoreY = false;
    public bool ignoreZ = true;

    public FloatVariable XChildPositionModifies = null;
    public FloatVariable YChildPositionModifies = null;
    public FloatVariable ZChildPositionModifies = null;


    public bool XChildModifier_negate = false;
    public bool YChildModifier_negate = false;
    public bool ZChildModifier_negate = false;

    private Vector3[] childDefaultLocalPositions;
    private Transform[] childTransforms;
    // Start is called before the first frame update
    void Start()
    {
        childTransforms = this.transform.GetComponentsInChildren<Transform>();

        childDefaultLocalPositions = childTransforms.Select(x => x.localPosition).ToArray();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 newPosition = SourceTransform.position;

        if (ignoreX)
            newPosition.x = this.transform.position.x;
        if (ignoreY)
            newPosition.y = this.transform.position.y;
        if (ignoreZ)
            newPosition.z = this.transform.position.z;

        this.transform.position = newPosition;

        Vector3 newChildPositon = Vector3.zero;

        if (XChildPositionModifies != null)
            newChildPositon.x = XChildModifier_negate ? -XChildPositionModifies.Value : XChildPositionModifies.Value;
        if (YChildPositionModifies != null)
            newChildPositon.y = YChildModifier_negate ? -YChildPositionModifies.Value : YChildPositionModifies.Value;
        if (ZChildPositionModifies != null)
            newChildPositon.z = ZChildModifier_negate ? -ZChildPositionModifies.Value : ZChildPositionModifies.Value;


        for (int i = 1; i < childTransforms.Length; i++)
            childTransforms[i].localPosition = childDefaultLocalPositions[i] + newChildPositon;
    }
}
