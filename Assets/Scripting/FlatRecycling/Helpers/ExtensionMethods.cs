﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public static class ExtensionMethods
{
    //public static float Remap(this float value, float from1, float to1, float from2, float to2)
    //{
    //    if (value < from1)
    //        return to1;

    //    if (value > from2)
    //        return to2;

    //    return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    //}

    public static float Remap(this float value, float origFrom, float origTo, float targetFrom, float targetTo)
    {
        float rel = Mathf.InverseLerp(origFrom, origTo, value);
        return Mathf.Lerp(targetFrom, targetTo, rel);
    }


    public static float RemapLogarithmic(this float value, float from1, float to1, float from2, float to2)
    {
        if (value < from1)
            return from2;

        if (value > to1)
            return to2;

        //return Mathf.Lerp(,)
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }

    public static Vector3Int ToGrid(this Vector3 original)
    {
        return new Vector3Int((int)Math.Floor(original.x), (int)Math.Floor(original.y), (int)Math.Floor(original.z));
    }
}

