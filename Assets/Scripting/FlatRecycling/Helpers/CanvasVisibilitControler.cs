﻿using UnityEngine;

public class CanvasVisibilitControler : MonoBehaviour
{
    private Canvas canvas;

    public BoolVariable Visible;

    public bool ForceStateOnstart = false;
    public bool StateAtStart = true;
    
    void Start()
    {
        canvas = this.GetComponent<Canvas>();

        if (ForceStateOnstart)
            Visible.Value = StateAtStart;
    }
    
    void Update()
    {
        canvas.enabled = Visible.Value;
    }
}
