﻿using TMPro;
using UnityEngine;

public class CanvasTextSetter : MonoBehaviour
{
    public StringVariable textVariable;
    private TextMeshProUGUI TextMesh;

    void Start()
    {
        TextMesh = this.GetComponent<TextMeshProUGUI>();

    }

    void Update()
    {
        if (textVariable != null && textVariable.Value != null)
            if (!TextMesh.text.Equals(textVariable.Value))
                TextMesh.text = textVariable.Value;
    }
}
