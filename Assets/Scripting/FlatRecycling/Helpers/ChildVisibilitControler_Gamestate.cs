﻿using UnityEngine;

public class ChildVisibilitControler_Gamestate : MonoBehaviour
{
    public GameStateVariable GameStateVariable;

    public GameState VisibleWhen = GameState.Loss;

    public bool ForceStateOnstart = false;
    public bool StateAtStart = true;

    void Start()
    {
    }

    void Update()
    {
        foreach (Transform child in this.transform)
            child.gameObject.SetActive(GameStateVariable.Value == VisibleWhen);
    }
}
