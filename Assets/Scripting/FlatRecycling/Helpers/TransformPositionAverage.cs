﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformPositionAverage : MonoBehaviour
{
    public Transform[] TargetTransforms;

    public bool lockX = true;
    public bool lockY = false;
    public bool lockZ = true;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 average = Vector3.zero;

        foreach (Transform transform in TargetTransforms)
            average += transform.position;

        average /= TargetTransforms.Length;

        Vector3 newPosition = this.transform.position;

        if (!lockX)
            newPosition.x = average.x;
        if (!lockY)
            newPosition.y = average.y;
        if (!lockZ)
            newPosition.z = average.z;

        this.transform.position = newPosition;
    }
}
