﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IsSeenIndicator : MonoBehaviour
{

    Image imageRend = null;

    public Sprite seen = null;
    public Sprite notSeen = null;

    public BoolVariable isSeen = null;

    // Start is called before the first frame update
    void Start()
    {
        imageRend = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isSeen.Value)
            imageRend.sprite = seen;
        else
            imageRend.sprite = notSeen;
    }
}
