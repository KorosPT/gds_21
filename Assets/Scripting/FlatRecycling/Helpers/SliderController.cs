﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderController : MonoBehaviour
{
    public IntVariable amount = null;
    public IntVariable maxValue = null;
    private Slider slider = null;


    // Start is called before the first frame update
    void Start()
    {
        slider = GetComponent<Slider>();
        slider.value = 0;
    }

    // Update is called once per frame
    void Update()
    {
        slider.value = Mathf.Min((float)(amount.Value / (double)maxValue.Value), 1);
    }
}
