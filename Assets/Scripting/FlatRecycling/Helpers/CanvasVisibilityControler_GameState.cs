﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasVisibilityControler_GameState : MonoBehaviour
{
    private Canvas canvas;

    public GameStateVariable GameStateVariable;

    public GameState VisibleWhen = GameState.Loss;

    public bool ForceStateOnstart = false;
    public bool StateAtStart = true;

    void Start()
    {
        canvas = this.GetComponent<Canvas>();
    }

    void Update()
    {
        canvas.enabled = GameStateVariable.Value == VisibleWhen;
    }
}
