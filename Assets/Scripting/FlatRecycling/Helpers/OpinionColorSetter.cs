﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpinionColorSetter : MonoBehaviour
{
    private SpriteRenderer sprite = null;
    private SpriteRenderer frame = null;

    public Color inactive = Color.white;
    public Color active1 = Color.red;
    public Color active2 = Color.blue;

    public double opinion = 0;

    // Start is called before the first frame update
    void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
        frame = transform.GetChild(0).GetComponent<SpriteRenderer>();
    }


    // Update is called once per frame
    void Update()
    {
        //if (opinion != 0)
        //    Debug.Log(opinion);

        if (opinion > 0)
        {
            sprite.color = new Color(active1.r, active1.g, active1.b, Mathf.Min(0.2f + ((float)opinion / 100), 1f));// Blend(active1, inactive, opinion / 100);
            //sprite.color.a = opinion / 100;
        }
        else if (opinion < 0)
        {
            //sprite.color = active2;// Blend(active2, inactive, -opinion / 100);
            sprite.color = new Color(active2.r, active2.g, active2.b, Mathf.Min(0.2f + ((float)-opinion / 100), 1f));
        }
        else
        {
            sprite.color = new Color(inactive.r, inactive.g, inactive.b, 0); ;
        }

        if (opinion >= 100 || opinion <= -100)
            if (frame != null)
                frame.enabled = false;

    }

    /// <summary>Blends the specified colors together.</summary>
    /// <param name="color">Color to blend onto the background color.</param>
    /// <param name="backColor">Color to blend the other color onto.</param>
    /// <param name="amount">How much of <paramref name="color"/> to keep,
    /// “on top of” <paramref name="backColor"/>.</param>
    /// <returns>The blended colors.</returns>
    public Color Blend(Color color, Color backColor, double amount)
    {
        byte r = (byte)((color.r * amount) + backColor.r * (1 - amount));
        byte g = (byte)((color.g * amount) + backColor.g * (1 - amount));
        byte b = (byte)((color.b * amount) + backColor.b * (1 - amount));
        return new Color(r, g, b);
    }

    public void setOpinion(int op)
    {
        this.opinion = op;
    }
}
