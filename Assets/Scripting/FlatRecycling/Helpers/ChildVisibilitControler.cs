﻿using UnityEngine;

public class ChildVisibilitControler : MonoBehaviour
{
    public BoolVariable Visible;

    public bool ForceStateOnstart = false;
    public bool StateAtStart = true;

    void Start()
    {
        if (ForceStateOnstart)
            Visible.Value = StateAtStart;
    }

    void Update()
    {
        foreach (Transform child in this.transform)
            child.gameObject.SetActive(Visible.Value);
    }
}
