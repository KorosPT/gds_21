﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageSwapper : MonoBehaviour
{
    Image imageRend = null;

    public Sprite pressed = null;
    public Sprite notPressed = null;

    public BoolVariable isPressed = null;

    // Start is called before the first frame update
    void Start()
    {
        imageRend = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isPressed.Value)
            imageRend.sprite = pressed;
        else
            imageRend.sprite = notPressed;
    }
}
