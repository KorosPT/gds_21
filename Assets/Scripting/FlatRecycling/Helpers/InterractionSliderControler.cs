﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InterractionSliderControler : MonoBehaviour
{
    private Slider slider = null;
    private Image bcg = null;
    private float targetValue = 0f;
    private float currentValue = 0f;

    //private float maxValue = 100;


    // Start is called before the first frame update
    void Start()
    {
        slider = GetComponent<Slider>();
        bcg = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        if (targetValue != 0 && currentValue != 0)
        {
            bcg.enabled = true;
            slider.enabled = true;
            slider.value = (currentValue / targetValue);
        }
        else
        {
            slider.value = 0;
            slider.enabled = false;
            bcg.enabled = false;
        }
    }

    internal void SetValues(float timeHolding, float timeForActivation)
    {
        currentValue = timeHolding;
        targetValue = timeForActivation;
    }
}
