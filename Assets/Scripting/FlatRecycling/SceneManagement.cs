using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SM = UnityEngine.SceneManagement.SceneManager;

public class SceneManagement : MonoBehaviour
{
    //public ScheduleManager schedules = null;
    //public TeamContainer teamsManager = null;

    //public BoolVariable paused = null;
    //public BoolVariable gameOver = null;

    public bool paused = false;

    public GameObject pauseMenu;

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1;
        //schedules.Start();
        //teamsManager.Start();
        //gameOver.Value = false;
        //paused.Value = false;
    }

    // Update is called once per frame
    void Update()
    {
        //schedules?.update(Time.deltaTime);

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!paused) {
                Pause();
            } else {
                Unpause();
            }
        }

        //if (teamsManager.IsOver())
        //{
        //    gameOver.Value = true;
        //    WinL.Value = teamsManager.WinL();
        //    WinR.Value = teamsManager.WinR();
        //    Time.timeScale = 0;
        //}
    }

    public void ReatartScene()
    {
        //StartCoroutine(MyCoroutine());
        SM.LoadScene(SM.GetActiveScene().buildIndex);
    }
    public void Quit()
    {
#if UNITY_WEBGL
        SM.LoadScene(0);
#else
        Application.Quit();
#endif
    }


    public void Pause() {
        paused = true;
        pauseMenu.SetActive(true);
        Time.timeScale = 0;
    }

    public void Unpause()
    {
        Time.timeScale = 1;
        paused = false;
        pauseMenu.SetActive(false);
    }

    IEnumerator MyCoroutine()
    {

        //AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("SampleScene");

        // Wait until the asynchronous scene fully loads
        //while (!asyncLoad.isDone)
        //{
        yield return null;
        //}
    }
}
