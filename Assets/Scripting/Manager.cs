using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class Manager : MonoBehaviour
{
    public List<TheHand> handos;
    public List<Indicator> indicatoros;

    public UnityEvent topEvent;
    public UnityEvent botEvent;
    public UnityEvent rightEvent;
    public UnityEvent leftEvent;

    public RhythmManager rhythmManager = null;

    public GameStateVariable gameState = null;

    public FloatVariable MusicVolume = null;
    public FloatVariable SfxVolume = null;

    public Octosquid squid = null;

    public IntVariable lives = null;
    public StringVariable livesText = null;
    public IntVariable score = null;
    public StringVariable scoreText = null;

    public AudioClip[] SlapSounds;
    public AudioClip[] WinSounds;
    public AudioClip[] LostSounds;
    public AudioClip[] HurtSounds;

    public FloatVariable inputCooldown;

    private AudioClip[] RandomizedHurtSounds;

    public SceneManagement sceneManagement = null;

    private int lastRememberedLives = 0;

    private Coroutine tickCoroutine;

    private AudioSource musicPlayer = null;
    private AudioSource hurtPlayer = null;
    private AudioSource SlapPlayer = null;
    private AudioSource otherPlayer = null;

    private float _inputCooldown;

    // Start is called before the first frame update
    void Start()
    {
        musicPlayer = gameObject.AddComponent<AudioSource>() as AudioSource;
        hurtPlayer = gameObject.AddComponent<AudioSource>() as AudioSource;
        SlapPlayer = gameObject.AddComponent<AudioSource>() as AudioSource;
        otherPlayer = gameObject.AddComponent<AudioSource>() as AudioSource;

        musicPlayer.volume = MusicVolume.Value;
        musicPlayer.loop = rhythmManager.FullRandomization;
        RandomizedHurtSounds = HurtSounds.OrderBy(x => UnityEngine.Random.value).ToArray();

        if (!rhythmManager.FullRandomization)
            musicPlayer.clip = rhythmManager.rhythm.song;
        else
            musicPlayer.clip = rhythmManager.song;

        rhythmManager.Initialize();
        gameState.Run();

        if (tickCoroutine != null)
            StopCoroutine(tickCoroutine);
        tickCoroutine = StartCoroutine(GameTick());
        lives.Value = 7;
        livesText.Value = "7";
        score.Value = 0;
        scoreText.Value = "0";
        lastRememberedLives = lives.Value;

        _inputCooldown = 0;
    }

    // Update is called once per frame
    void Update()
    {
        musicPlayer.volume = MusicVolume.Value;
        livesText.Value = lives.Value.ToString();
        scoreText.Value = score.Value.ToString();

        if (musicPlayer.isPlaying && gameState.Value != GameState.Running)
            musicPlayer.Pause();

        if (!musicPlayer.isPlaying && gameState.Value == GameState.Running)
            musicPlayer.Play();


       if( lastRememberedLives != lives.Value)
        {
            hurtPlayer.volume = SfxVolume.Value;
            hurtPlayer.clip = RandomizedHurtSounds[lives.Value];
            hurtPlayer.Play();
        }

        lastRememberedLives = lives.Value;

        if (gameState.Value != GameState.Running)
            return;

        if (lives.Value <= 0)
            GameOver();

        if (_inputCooldown <= 0) {
            if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W)) {

                if (rhythmManager.rhythm.write)
                    rhythmManager.Record(direction.Up);
                //rhythmManager.UpContact.Value = true;
                //GAME LOGIC HERE//
                if (NeedAHand(HandType.TOP).movingIn)
                    AddScore();

                NeedAHand(HandType.TOP).MoveOut();
                squid.PlayTop();

                _inputCooldown = inputCooldown.Value;
            }

            if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S)) {

                if (rhythmManager.rhythm.write)
                    rhythmManager.Record(direction.Down);
                //rhythmManager.DownContact.Value = true;
                //GAME LOGIC HERE//
                if (NeedAHand(HandType.BOT).movingIn)
                    AddScore();

                NeedAHand(HandType.BOT).MoveOut();
                squid.PlayBot();

                _inputCooldown = inputCooldown.Value;
            }

            if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D)) {

                if (rhythmManager.rhythm.write)
                    rhythmManager.Record(direction.Right);
                //rhythmManager.RightContact.Value = true;
                //GAME LOGIC HERE//
                if (NeedAHand(HandType.RGT).movingIn)
                    AddScore();

                NeedAHand(HandType.RGT).MoveOut();
                squid.PlayRight();

                _inputCooldown = inputCooldown.Value;
            }

            if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A)) {

                if (rhythmManager.rhythm.write)
                    rhythmManager.Record(direction.Left);
                //rhythmManager.LeftContact.Value = true;
                //GAME LOGIC HERE//
                if (NeedAHand(HandType.LFT).movingIn)
                    AddScore();

                NeedAHand(HandType.LFT).MoveOut();
                squid.PlayLeft();

                _inputCooldown = inputCooldown.Value;
            }
        } else {
            _inputCooldown -= Time.deltaTime;
        }

        if (rhythmManager.rhythm.write)
            return;

            //Hands
            if (rhythmManager.UpContact.Value)
        {
            rhythmManager.UpContact.Value = false;
            topEvent.Invoke();
        }

        if (rhythmManager.DownContact.Value)
        {
            rhythmManager.DownContact.Value = false;
            botEvent.Invoke();
        }

        if (rhythmManager.LeftContact.Value)
        {
            rhythmManager.LeftContact.Value = false;
            leftEvent.Invoke();
        }

        if (rhythmManager.RightContact.Value)
        {
            rhythmManager.RightContact.Value = false;
            rightEvent.Invoke();
        }


        //Indicators
        if (rhythmManager.UpWarning.Value)
        {
            rhythmManager.UpWarning.Value = false;
            NeedIndicator(HandType.TOP).StartFillIn();
        }

        if (rhythmManager.DownWarning.Value)
        {
            rhythmManager.DownWarning.Value = false;
            NeedIndicator(HandType.BOT).StartFillIn();
        }

        if (rhythmManager.LeftWarning.Value)
        {
            rhythmManager.LeftWarning.Value = false;
            NeedIndicator(HandType.LFT).StartFillIn();
        }

        if (rhythmManager.RightWarning.Value)
        {
            rhythmManager.RightWarning.Value = false;
            NeedIndicator(HandType.RGT).StartFillIn();
        }

        //foreach (Indicator ind in indicatoros)
        //{
        //    TheHand hand = NeedAHand(ind.type);
        //    ind.fill = hand.lerp;
        //}
    }

    private void AddScore()
    {
        score.Value++;

        SlapPlayer.volume = SfxVolume.Value/2;
        SlapPlayer.clip = SlapSounds[UnityEngine.Random.Range(0, SlapSounds.Length)];
        SlapPlayer.Play();
    }

    private void GameOver()
    {
        StopCoroutine(tickCoroutine);
        gameState.Value = GameState.Loss;

        otherPlayer.volume = SfxVolume.Value;
        otherPlayer.clip = LostSounds[UnityEngine.Random.Range(0, LostSounds.Length)];
        otherPlayer.Play();

        //sceneManagement.Pause();
        Time.timeScale = 0;
    }

    private TheHand NeedAHand(HandType type)
    {
        return handos.Find(x => x.type == type);
    }

    private Indicator NeedIndicator(HandType type)
    {
        return indicatoros.Find(x => x.type == type);
    }


    IEnumerator GameTick()
    {
        while (!rhythmManager.RhythmOver())
        {
            if (rhythmManager.FullRandomization)
                yield return new WaitForSeconds(rhythmManager.speedMS / 1000f);
            else
                yield return new WaitForSeconds(rhythmManager.rhythm.speedMS / 1000f);

            //Debug.Log(rhythmManager.index + ":  " + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff"));

            rhythmManager.Tick();

            direction currDir = rhythmManager.currentValue;
            direction prediction = rhythmManager.predictedValue;

            switch (prediction)
            {
                case direction.None:
                    break;
                case direction.Up:
                    rhythmManager.UpWarning.Value = true;
                    break;
                case direction.Down:
                    rhythmManager.DownWarning.Value = true;
                    break;
                case direction.Left:
                    rhythmManager.LeftWarning.Value = true;
                    break;
                case direction.Right:
                    rhythmManager.RightWarning.Value = true;
                    break;
                default:
                    break;
            }

            switch (currDir)
            {
                case direction.None:
                    break;
                case direction.Up:
                    rhythmManager.UpContact.Value = true;
                    break;
                case direction.Down:
                    rhythmManager.DownContact.Value = true;
                    break;
                case direction.Left:
                    rhythmManager.LeftContact.Value = true;
                    break;
                case direction.Right:
                    rhythmManager.RightContact.Value = true;
                    break;
                default:
                    break;
            }
        }

        gameState.Value = GameState.Victory;
        otherPlayer.volume = SfxVolume.Value;
        otherPlayer.clip = WinSounds[UnityEngine.Random.Range(0, WinSounds.Length)];
        otherPlayer.Play();

        Time.timeScale = 0;

        //sceneManagement.Pause();
    }
}
