using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Octosquid : MonoBehaviour {
    public float idleDelay;
    public bool isIdle = true;

    public AnimationVariable anims;

    private float _currentDelay;
    private SpriteRenderer _renderer;
    private Coroutine _anim;

    // Start is called before the first frame update
    void Start() {
        //_currentDelay = idleDelay;
        _renderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update() {
        if (_currentDelay > 0) {
            _currentDelay -= Time.deltaTime;
            isIdle = false;
        } else if (isIdle == false) {
            isIdle = true;
            if (_anim != null) StopCoroutine(_anim);
            _anim = StartCoroutine(PlayLoop(anims.frameIdleDelay, anims.idleFrame1, anims.idleFrame2));
        }
    }

    public void PlayTop() {
        ChangeState(HandType.TOP);
    }

    public void PlayBot() {
        ChangeState(HandType.BOT);
    }

    public void PlayRight() {
        ChangeState(HandType.RGT);
    }

    public void PlayLeft() {
        ChangeState(HandType.LFT);
    }

    private void ChangeState(HandType direction) {
        _currentDelay = idleDelay;
        if (_anim != null) StopCoroutine(_anim);
        Smack(direction);
    }

    public void Smack(HandType direction) {
        Sprite frame1 = null, frame2 = null;

        switch(direction) {
            case HandType.TOP:
                frame1 = anims.smackTopFrame1;
                frame2 = anims.smackTopFrame2;
                break;
            case HandType.BOT:
                frame1 = anims.smackBotFrame1;
                frame2 = anims.smackBotFrame2;
                break;
            case HandType.RGT:
                frame1 = anims.smackRightFrame1;
                frame2 = anims.smackRightFrame2;
                break;
            case HandType.LFT:
                frame1 = anims.smackLeftFrame1;
                frame2 = anims.smackLeftFrame2;
                break;
        }

        _anim = StartCoroutine(PlayAnim(anims.frameSmackDelay, frame1, frame2));
    }

    public void BadTouch() {
        _currentDelay = idleDelay;
        if (_anim != null) StopCoroutine(_anim);
        _anim = StartCoroutine(PlayTouched(anims.frameSmackDelay * 2, anims.badTouchFrame));

    }

    private IEnumerator PlayTouched(float frameDelay, Sprite frame) {
        Vector3 original = this.transform.position;
        isIdle = true;
        _renderer.sprite = frame;
        float time = frameDelay;
        while (time > 0) {
            float x = Random.Range(-1f, 1f) * 0.1f;
            float y = Random.Range(-1f, 1f) * 0.1f;

            this.transform.position = original + new Vector3(x, y, original.z);

            time -= Time.deltaTime;
            yield return null;
        }
        this.transform.position = original;
        isIdle = false;
    }

    private IEnumerator PlayAnim(float frameDelay, Sprite frame1, Sprite frame2) {
        _renderer.sprite = frame1;
        float time = frameDelay;
        while (time > 0) {
            time -= Time.deltaTime;
            yield return null;
        }
        _renderer.sprite = frame2;
    }

    private IEnumerator PlayLoop(float frameDelay, Sprite frame1, Sprite frame2) {
        while(true) {
            _renderer.sprite = frame1;
            float time = frameDelay;
            while (time > 0) {
                time -= Time.deltaTime;
                yield return null;
            }
            _renderer.sprite = frame2;
            time = frameDelay;
            while (time > 0) {
                time -= Time.deltaTime;
                yield return null;
            }
        }
    }
}
