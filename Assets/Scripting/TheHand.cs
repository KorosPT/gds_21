using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TheHand : MonoBehaviour
{
    public Transform waypoint1;
    public Transform waypoint2;

    public HandType type;
    public float lerp
    {
        get { return _destination == waypoint2 ? 0 : _lastLerp; }
    }

    public FloatVariable MovementLengthMS = null;
    public IntVariable lives = null;

    public UnityEvent touched;

    private float _lastLerp = 1;
    private Coroutine _movement;
    private Transform _destination;

    internal bool movingIn = false;

    void Start()
    {
        MoveOut(0.1f, 0f);
    }

    void Update()
    {

        if (_lastLerp == 1f && movingIn)
        {
            movingIn = false;
            MoveOut(MovementLengthMS.Value / 1000f, 0f);
            lives.Value--;
            touched.Invoke();
        }
    }

    public void MoveIn(float time, float delay)
    {
        if (_movement != null)
        {
            StopCoroutine(_movement);
        }

        _destination = waypoint1;
        _movement = StartCoroutine(LerpPositionDelayed(waypoint1.position, time, delay));
      
        movingIn = true;
    }

    public void MoveOut(float time, float delay)
    {
        if (_movement != null)
        {
            StopCoroutine(_movement);
        }

        _destination = waypoint2;
        _movement = StartCoroutine(LerpPositionDelayed(waypoint2.position, time , delay));
        movingIn = false;
    }

    public void ToggleMovement()
    {
        if (_destination == waypoint1)
        {
            MoveOut(MovementLengthMS.Value / 1000f, 0f);
        }
        else
        {
            MoveIn(MovementLengthMS.Value / 1000f, 0f);
        }
    }

    private IEnumerator LerpPositionDelayed(Vector2 targetPosition, float fullDuration, float delay)
    {
        float time = 0;
        Vector2 startPosition = transform.position;
        float duration = Remap(fullDuration, 0f, Math.Abs(Vector2.Distance(waypoint1.position, waypoint2.position)), 0f, Math.Abs(Vector2.Distance(startPosition, targetPosition)));

        while (delay > 0)
        {
            delay -= Time.deltaTime;
        }

        while (time < duration)
        {
            _lastLerp = time / duration;
            transform.position = Vector2.Lerp(startPosition, targetPosition, _lastLerp);
            time += Time.deltaTime;
            yield return null;
        }
        transform.position = targetPosition;
        _lastLerp = 1;
    }


    public void MoveOut()
    {
        MoveOut(MovementLengthMS.Value / 1000f, 0f);
    }

    public void MoveIn()
    {
        MoveIn(MovementLengthMS.Value / 1000f, 0f);
    }


    public float Remap(float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }
}

public enum HandType
{
    TOP, BOT, RGT, LFT
}
