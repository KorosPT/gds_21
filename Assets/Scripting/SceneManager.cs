using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneManager : MonoBehaviour
{
    public RhythmManager rhythmManager = null;

    public GameStateVariable gameState = null;

    public FloatVariable MusicVolume = null;
    public FloatVariable SfxVolume = null;

    private Coroutine tickCoroutine;

    private AudioSource musicPlayer = null;

    // Start is called before the first frame update
    void Start()
    {
        musicPlayer = GetComponent<AudioSource>();
        musicPlayer.volume = MusicVolume.Value;

        if (!rhythmManager.FullRandomization)
            musicPlayer.clip = rhythmManager.rhythm.song;
        else
            musicPlayer.clip = rhythmManager.song;

        rhythmManager.Initialize();
        gameState.Run();

        if (tickCoroutine != null)
            StopCoroutine(tickCoroutine);
        tickCoroutine = StartCoroutine(GameTick());
    }

    // Update is called once per frame
    void Update()
    {
        if (musicPlayer.isPlaying && gameState.Value != GameState.Running)
            musicPlayer.Pause();

        if (!musicPlayer.isPlaying && gameState.Value == GameState.Running)
            musicPlayer.Play();


        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            if (rhythmManager.rhythm.write)
                rhythmManager.Record(direction.Up);
            rhythmManager.UpContact.Value = true;
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (rhythmManager.rhythm.write)
                rhythmManager.Record(direction.Down);
            rhythmManager.DownContact.Value = true;
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (rhythmManager.rhythm.write)
                rhythmManager.Record(direction.Right);
            rhythmManager.RightContact.Value = true;
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (rhythmManager.rhythm.write)
                rhythmManager.Record(direction.Left);
            rhythmManager.LeftContact.Value = true;
        }

    }

    IEnumerator GameTick()
    {
        while (!rhythmManager.RhythmOver())
        {
            if (rhythmManager.FullRandomization)
                yield return new WaitForSeconds(rhythmManager.speedMS / 1000f);
            else
                yield return new WaitForSeconds(rhythmManager.rhythm.speedMS / 1000f);

            //Debug.Log(rhythmManager.index + ":  " + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff"));

            rhythmManager.Tick();

            direction currDir = rhythmManager.currentValue;
            direction prediction = rhythmManager.predictedValue;

            switch (prediction)
            {
                case direction.None:
                    break;
                case direction.Up:
                    rhythmManager.UpWarning.Value = true;
                    break;
                case direction.Down:
                    rhythmManager.DownWarning.Value = true;
                    break;
                case direction.Left:
                    rhythmManager.LeftWarning.Value = true;
                    break;
                case direction.Right:
                    rhythmManager.RightWarning.Value = true;
                    break;
                default:
                    break;
            }

            switch (currDir)
            {
                case direction.None:
                    break;
                case direction.Up:
                    rhythmManager.UpContact.Value = true;
                    break;
                case direction.Down:
                    rhythmManager.DownContact.Value = true;
                    break;
                case direction.Left:
                    rhythmManager.LeftContact.Value = true;
                    break;
                case direction.Right:
                    rhythmManager.RightContact.Value = true;
                    break;
                default:
                    break;
            }
        }

        gameState.Value = GameState.Victory;
    }
}
