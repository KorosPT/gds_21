using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "RhythmManager")]
public class RhythmManager : ScriptableObject
{
    public BoolVariable LeftContact = null;
    public BoolVariable RightContact = null;
    public BoolVariable UpContact = null;
    public BoolVariable DownContact = null;


    public BoolVariable LeftWarning = null;
    public BoolVariable RightWarning = null;
    public BoolVariable UpWarning = null;
    public BoolVariable DownWarning = null;

    public RhythmVariable rhythm = null;

    public bool FullRandomization = false;
    public int speedMS = 1000;
    public int futureVision = 8;
    public int reactionWindow = 8;
    public float initSpawnChance = .1f;
    public float spawnChance = .1f;
    public float spawnIncrease = .01f;
    public string randomWhythm = string.Empty;
    public AudioClip song = null;

    public direction currentValue = direction.None;
    public direction predictedValue = direction.None;

    public FloatVariable WarningLengthMS = null;
    public FloatVariable ReactionWindowLengthMS = null;

    private direction recordedValue = direction.None;
    public int index = 0;

    public int gracePeriod = 10;
    public int currGracePeriod = 10;

    public void Initialize()
    {
        if (rhythm.write)
            rhythm.Value = string.Empty;
        index = 0;
        spawnChance = initSpawnChance;
        currGracePeriod = gracePeriod;

        if (FullRandomization)
        {
            randomWhythm = new string('0', futureVision + 1);
            WarningLengthMS.Value = futureVision * speedMS;
            ReactionWindowLengthMS.Value = reactionWindow * speedMS;
        }
        else
        {
            WarningLengthMS.Value = rhythm.futureVision * rhythm.speedMS;
            ReactionWindowLengthMS.Value = rhythm.reactionWindow * rhythm.speedMS;
        }
    }

    internal void Record(direction dir)
    {
        recordedValue = dir;
    }

    public bool RhythmOver()
    {
        if (!FullRandomization)
            return !rhythm.write && (index >= rhythm.Value.Length-1);
        else
            return false;
    }

    internal void Tick()
    {
        if (rhythm.write)
        {
            rhythm.Value += ((int)recordedValue).ToString();

            recordedValue = direction.None;
            return;
        }

        if (FullRandomization)
        {
            if(spawnChance <= 1.2)
                spawnChance += spawnIncrease;
            else if (currGracePeriod > 0)
            {
                spawnChance = 1f;
                currGracePeriod--;
            }


            while (index + futureVision + 1 > randomWhythm.Length)
            {
                if (UnityEngine.Random.value < spawnChance)
                    randomWhythm += UnityEngine.Random.Range(1, 5).ToString() + new string('0', currGracePeriod);
                else
                    randomWhythm += "0";
            }

            predictedValue = (direction)char.GetNumericValue(randomWhythm[index + futureVision]);
            currentValue = (direction)char.GetNumericValue(randomWhythm[index++]);

            return;
        }

        recordedValue = direction.None;

        if (index + rhythm.futureVision + 1 < rhythm.Value.Length)
            predictedValue = (direction)char.GetNumericValue(rhythm.Value[index + rhythm.futureVision]);
        else
            predictedValue = direction.None;

        if (index + 1 < rhythm.Value.Length)
            currentValue = (direction)char.GetNumericValue(rhythm.Value[index++]);
        else
            currentValue = direction.None;


        return;
    }
}

public enum direction
{
    None = 0,
    Up = 1,
    Down = 2,
    Left = 3,
    Right = 4
}
